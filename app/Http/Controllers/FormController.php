<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('page.form');
    }

    public function send(Request $request){
        $namapertama = $request -> first;
        $namaterakhir = $request -> last;
        $email = $request ->em;
        $jenis_kelamin = $request -> gender;
        $kebangsaan = $request -> wn;
        $bahasa = $request -> bahasa;
        $biodata = $request ->biodata;
        return view('page.selamat', compact('namapertama','namaterakhir','email','jenis_kelamin','kebangsaan','bahasa','biodata'));
    }
}
