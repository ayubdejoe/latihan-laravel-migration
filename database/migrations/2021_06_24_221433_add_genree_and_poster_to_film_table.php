<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenreeAndPosterToFilmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film', function (Blueprint $table) {
            $table->unsignedBigInteger('genree_id');
            $table->foreign('genree_id')->references('id')->on('genree')->onDelete('cascade');
            $table->string('poster');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film', function (Blueprint $table) {
            $table->dropForeign(['genree_id']);
            $table->dropColumn('genree_id');
            $table->dropColumn('poster');
        });
    }
}
