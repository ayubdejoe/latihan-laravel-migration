@extends('layout.master')

@section('judul1')
    SELAMAT DATANG {{$namapertama }} {{$namaterakhir}}
@endsection

@section('judul2')
    Terima kasih telah bergabung di SanberBook. Social Media kita bersama! <br>
    Berikut adalah Biodata singkat kamu
@endsection

@section('isi')
    Nama Lengkap = {{$namapertama}} {{$namaterakhir}} <br>
    Email = {{$email}} <br>
    Jenis Kelamin = {{$jenis_kelamin}} <br>
    Negara Asal = {{$kebangsaan}} <br>
    Bahasa = {{$bahasa}} <br>
    Bio Singkatmu = {{$biodata}} <br>
    <input href="/index" type="submit" value="Finish">
@endsection

