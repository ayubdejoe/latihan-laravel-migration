@extends('layout.master')

@section('judul1')
    Buat Account Baru!
@endsection

@section('judul2')
    Sign Up Form
@endsection

@section('isi')
    {{-- <a href="/form">Registrasi</a> --}}
    <form action="/post" method="POST">
        @csrf
        <label for="fn">First name:</label><br>
        <input type="text" name="first" id="fn"><br>
        <label for="ln">Last name:</label><br>
        <input type="text" name="last" id="ln"><br>
        <label for="email">Email:</label><br>
        <input type="text" name="em" id="email" required>
        <br><br>
        <label for="gender">Gender:</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>
        <br><br>
        <label for="wn">Nationality:</label><br>
        <select name="wn" id="wn">
            <option value="+62">Indonesia</option>
            <option value="+xx">Others</option>
        </select>
        <br><br>
        <label for="bahasa">Language Spoken:</label><br>
        <input type="checkbox" name="bahasa" id="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" id="bahasa">English <br>
        <input type="checkbox" name="bahasa" id="bahasa">Others <input type="text" name="bahasa" id="bahasa"><br>
        <br><br>
        <label for="bio">Bio</label><br>
        <textarea name="biodata" id="bio" cols="30" rows="10"></textarea><br>
        <input href="/selamat" type="submit" value="Kirim">
        <br><br>
    </form>
@endsection
    
