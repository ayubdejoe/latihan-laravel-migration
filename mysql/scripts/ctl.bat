@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop


"D:\amp\mysql\bin\mysqld" --defaults-file="D:\amp\mysql\bin\my.ini" --standalone
if errorlevel 1 goto error
goto finish

:stop
cmd.exe /C start "" /MIN call "D:\amp\killprocess.bat" "mysqld.exe"

if not exist "D:\amp\mysql\data\%computername%.pid" goto finish
echo Delete %computername%.pid ...
del "D:\amp\mysql\data\%computername%.pid"
goto finish


:error
echo MySQL could not be started

:finish
exit
